#ifndef GRAF_H_INCLUDED
#define GRAF_H_INCLUDED

struct node
{
    int dest;
    node* next;
};

struct adjacencyList
{
    node* head;
};

struct graph
{
    int vertices;
    adjacencyList* listArray;
};

node* createNewNode(const int);
graph* createGraph(const int);
void addEdge(graph*, const int, const int);

#endif // GRAF_H_INCLUDED
