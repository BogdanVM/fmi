#include <fstream>
#include <iostream>
#include <queue>
#include <vector>
#include <climits>

#include "graf.h"

using namespace std;


node* createNewNode(const int dest)
{
    /* Cream noul nod si il returnam */
    node* newNode = new node;
    newNode->dest = dest;
    newNode->next = NULL;

    return newNode;
}

graph* createGraph(const int vertices)
{
    /* Alocam memorie pentru noul graf */
    graph* newGraph = new graph;
    newGraph->vertices = vertices;

    /* Alocam memorie pentru listele de adiacenta retinute in graf */
    newGraph->listArray = new adjacencyList[vertices];

    /* Initializam fiecare inceput de lista din graf cu nul */
    for (int i = 0; i < vertices; i++)
        newGraph->listArray[i].head = NULL;

    return newGraph;
}

void addEdge(graph* myGraph, const int source, const int destination)
{
    /* Adaugam nodul destinatie in lista de adiacenta, la inceputul acesteia */
    node* newNode = createNewNode(destination);
    newNode->next = myGraph->listArray[source].head;
    myGraph->listArray[source].head = newNode;
}

void printGraph(graph* myGraph)
{
    for(int i = 0; i < myGraph->vertices; i++)
    {
        node* currentNode = myGraph->listArray[i].head;
        cout<<"\nVecinii nodului "<<i + 1<<" sunt: ";

        while (currentNode)
        {
            cout<<currentNode->dest + 1<<" ";
            currentNode = currentNode->next;
        }

        cout<<"\n";
    }
}

void readList(ifstream& fin, graph* myGraph, const int vertices, vector<int>& finalNodes)
{

    for(int i = 0; i < vertices; i++)
    {
        char auxChar = ' ';

        /* Realizam citirea vecinilor, dupa fiecare vecin citind un caracter.
        Cand acest caracter este egal cu '\n' inseamna ca am terminat vecinii nodului i */
        while (auxChar != '\n')
        {
            int neighbour;
            fin>>neighbour;

            addEdge(myGraph, i, neighbour - 1);

            auxChar = fin.get();
        }
    }

    /* Formam vectorul cu punctele de control */
    int number;
    while(fin>>number)
        finalNodes.push_back(number - 1);

    fin.close();
}

void findShortestPath(const int startNode, const int finalNode, vector<int> parents)
{
    /* Daca avem acelasi nod de inceput si de sfarsit
    sau nu exista legatura intre ele, afisam nodul de inceput */
    if( startNode == finalNode || finalNode == -1)
        cout<<startNode + 1<<" ";

    else
    {
        /* Avansam in vectorul de tati */
        findShortestPath(startNode, parents[finalNode], parents);
        cout<<finalNode + 1<<" ";
    }
}

int findMinimumNode(vector<int> distances, vector<int> finalNodes)
{
    int minimumDistance = INT_MAX;
    int minimumDistanceNode = -1;

    /* Parcurgem toate punctele de control */
    for(vector<int>::iterator it = finalNodes.begin(); it != finalNodes.end(); ++it)
    {
        int currentFinalNode = (*it);

        /* Daca distanta catre nodul final nu a fost modificata, trecem la urmatorul punct de control */
        if (distances[currentFinalNode] == INT_MAX) continue;

        /* Calculam distanta minima catre un punct de control */
        if (distances[currentFinalNode] < minimumDistance)
        {
            minimumDistance = distances[currentFinalNode];
            minimumDistanceNode = currentFinalNode;
        }
    }

    return minimumDistanceNode;
}

void printVector(vector<int> whatever)
{
    for(vector<int>::iterator it = whatever.begin(); it != whatever.end(); it++)
        cout<<*it<<" ";
}

void BFS(graph* myGraph, const int startNode, vector<int> finalNodes)
{
    const int vertices = myGraph->vertices;

    /* Declaram 2 vectori in care vom retine pas cu pas distantele, respectiv "parintii" fiecarui nod */
    vector<int> distances (vertices, INT_MAX);
    vector<int> parents (vertices, -1);

    /* Initializam distanta de la primul nod la el insusi care este 0.
       Declaram o coada in care vom retine nodurile parcurse pas cu pas.
       Adaugam in coada nodul de inceput */
    distances[startNode] = 0;
    queue<int> myQueue;
    myQueue.push(startNode);

    /* Parcurgem elementele din coada cat timp aceasta nu este goala */
    while (!myQueue.empty())
    {
        /* Extragem primul element din coada si apoi il stergem */
        int currentNode = myQueue.front();
        myQueue.pop();

        /* Extragem nodul din capul listei de adiacenta a nodului curent */
        node* currentNeighbour = myGraph->listArray[currentNode].head;
        while (currentNeighbour)
        {
            /* Daca distanta de la start la nodul curent este cea initiala, atunci o modificam */
            if (distances[currentNeighbour->dest] == INT_MAX)
            {
                /* Adaugam noul nod in coada si crestem distanta corespunzatoare si ii marcam "parintele" */
                myQueue.push(currentNeighbour->dest);
                distances[currentNeighbour->dest] = distances[currentNode] + 1;
                parents[currentNeighbour->dest] = currentNode;
            }

            currentNeighbour = currentNeighbour->next;
        }
    }

    /* Daca exista cel putin o cale pana la nodul final, o gasim pe cea mai scurta si o afisam */
    int minimumFinalNode = findMinimumNode(distances, finalNodes);
    if (minimumFinalNode == -1)
        cout<<"\nNu exista nicio cale de la nodul "<<startNode + 1<<" la vreun punct de control\n";

    else{
        cout<<"\nCalea cea mai scurta de la nodul "<<startNode + 1<<" la punctul de control "<<minimumFinalNode + 1<<" este: ";
        findShortestPath(startNode, minimumFinalNode, parents);
        cout<<"\n";
    }

}

int main()
{
    ifstream fin("graf.in");

    int vertices;
    graph* myGraph;

    fin>>vertices;
    myGraph = createGraph(vertices);

    vector<int> finalNodes;
    readList(fin, myGraph, vertices, finalNodes);

    int startNode;
    cout<<"\nNodul care trebuie conectat cu un punct de control este: ";
    cin>>startNode;

    BFS(myGraph, startNode - 1, finalNodes);

    return 0;
}
