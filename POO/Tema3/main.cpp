#include <bits/stdc++.h>
#include "Classes/Headers/Gestiune.h"
#include "Classes/Headers/Card.h"
#include "Classes/Headers/Cec.h"
#include "Classes/Headers/Cash.h"

template <class T>
int Gestiune<T>::nrPlati = 0;

void displayMenu() {
    std::cout << "\nURMATOAREA PLATA ESTE DE TIP: ";
    std::cout << "\n1)  CASH";
    std::cout << "\n2)  CARD";
    std::cout << "\n3)  CEC\n";
    std::cout << "Introduceti optiunea aleasa: ";
}

int main() {
    Gestiune<Plata> payments; // sablonul
    Gestiune<std::string> clients; // specializarea

    int n;
    std::cout << "\nIntroduceti numarul de plati efectuate: ";
    std::cin >> n;

    for (int i = 0; i < n; i++){
        /* Citim de fiecare data o optiune, pentru a stabili ce fel de plata vom adauga */
        int option;
        displayMenu();
        std::cin >> option;

        switch (option) {
            case 1: {
                Cash* cash = new Cash;
                std::cin >> (*cash); // citim plata de tip cash

                payments += (*cash);
                break;
            }

            case 2: {
                Card card;
                std::cin >> card;
                payments += card;
                clients = clients + card.getName();
                break;
            }

            case 3: {
                Cec cec;
                std::cin >> cec;
                payments += cec;
                clients = clients + cec.getName();
                break;
            }
        }
    }

    std::vector<Plata*> paymentsVector = payments.getPlati();
    for (auto it : paymentsVector) {
        /* Parcurgem fiecare plata si vedem de ce tip e */
        if (Card* card = dynamic_cast<Card*>(it)) {
            std::cout << "CARD:\n" << *card;
        }

        else if (Cash* cash = dynamic_cast<Cash*>(it)) {
            std::cout << "CASH:\n" << *cash;
        }

        else if (Cec* cec = dynamic_cast<Cec*>(it)) {
            std::cout << "CEC:\n" << *cec;
        }
    }
    return 0;
}