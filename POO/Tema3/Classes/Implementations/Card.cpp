//
// Created by Bogdan on 5/4/2018.
//

#include <bits/stdc++.h>
#include "../Headers/Card.h"
#include "../Headers/Functions.h"

/*  Utility methods  */

std::ostream& Card::display(std::ostream& o) const {
    Plata::display(o);
    o << "Numarul cardului: " << number << "\n";
    o << "Numele platitorului: " << name << "\n";
    return o;
}

/*  Operator overloads  */

std::istream& operator >> (std::istream& in, Card& card) {

    Plata plata;
    in >> plata;

    card.date = plata.getDate();
    card.sum = plata.getSum();

    std::string s;

    do {

        try {
            std::cout << "Introduceti numarul cardului: ";
            in >> s;
            if (!Functions::validCard(s)){
                throw std::runtime_error("NUMARUL INTRODUS NU ESTE VALID");
            }

            break;

        } catch (std::runtime_error error) {
            std::cout << "\nA aparut o eroare la citire: \n" << error.what() << "\n";
            continue;
        }

    } while (1);

    card.number = s;

    do {
        try{

            std::cout << "Introduceti numele platitorului: ";
            std::getline(in >> std::ws, s);

            if (!Functions::validName(s)) {
                throw std::runtime_error("NUMELE INTRODUS NU ESTE VALID");
            }

            break;

        } catch (std::runtime_error error) {
            std::cout << "\nA aparut o eroare la citire:\n" << error.what() << "\n";
            continue;
        }
    } while (1);

    card.name = s;
    return in;
}

std::ostream& operator << (std::ostream& o, const Card& card) {
    return card.display(o);
}

const Card& Card::operator=(const Card& card) {

    if (this != &card) {
        date = card.date;
        sum = card.sum;
        number = card.number;
    }
    return *this;
}

