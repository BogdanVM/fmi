//
// Created by Bogdan on 5/4/2018.
//

#include "../Headers/Cec.h"
#include "../Headers/Functions.h"

/*   Utility methods  */
std::ostream& Cec::display(std::ostream& o) const {
    Plata::display(o);
    o << "Numele platitorului: " << name << "\n";
    return o;
}

/*   Operator overloads  */
std::istream& operator >> (std::istream& in, Cec& cec) {
    std::string s;

    Plata plata;
    in >> plata;

    cec.date = plata.getDate();
    cec.sum = plata.getSum();

    do {
        try{
            std::cout << "\nIntroduceti numele platitorului: ";
            std::getline(in >> std::ws, s);

            if (!Functions::validName(s)) {
                throw std::runtime_error("NUMELE INTRODUS NU ESTE VALID");
            }

            break;

        } catch (std::runtime_error error) {
            std::cout << "\nA aparut o eroare la citire:\n" << error.what() << "\n";
            continue;
        }
    } while (1);

    cec.name = s;
    return in;
}

std::ostream& operator << (std::ostream& o, const Cec& cec) {
    return cec.display(o);
}

const Cec& Cec::operator=(const Cec& cec) {
    if (this != &cec) {
        date = cec.date;
        sum = cec.sum;
        name = cec.name;
    }
    return *this;
}