//
// Created by Bogdan on 5/2/2018.
//

#include <bits/stdc++.h>
#include "../Headers/Plata.h"
#include "../Headers/Functions.h"

/*  UTILITY METHODS  */

std::ostream& Plata::display(std::ostream& o) const {
    o << "\nPlata in valoare de " << sum << " a fost efectuata in data de " << date;
    o << "\n";
    return o;
}

/*  OPERATOR OVERLOADS  */
std::istream& operator >> (std::istream& in, Plata& p) {

    std::string s;
    double x;

    do {
        try {
            std::cout << "\nIntroduceti data cand a fost efectuata plata (format DD/MM/YYYY): ";
            in >> s;

            /*  Daca nr de caractere difera de formatul acceptat, aruncam o exceptie  */
            if (s.size() != 10) {
                throw std::runtime_error("FORMAT INCORECT. DATA TREBUIE INTRODUSA ASTFEL: DD/MM/YYYY");
            }

            /*  Incercam sa extragem din sirul citit ziua, luna si anul.
             * Daca citirea nu s-a facut corect vom trata exceptiile in catch */
            unsigned int day = std::stoi(s.substr(0, 2));
            unsigned int month = std::stoi(s.substr(3, 2));
            unsigned int year = std::stoi(s.substr(6, 4));

            if (!Functions::correctDate(day, month, year)) {
                throw std::runtime_error("DATA NU ESTE VALIDA!");
            }

            break; // daca nu am aruncat nicio exceptie, incheiem executia do-while

        } catch (std::runtime_error error) {
            std::cout << "\nA avut loc o eroare la citire: \n" << error.what() << "\n";
            continue; // daca am prins o exceptie, reexecutam do while-ul pentru citire
        } catch (std::invalid_argument error) {
            std::cout << "\nData a fost specificata gresit: \n";
            continue;
        } catch (std::out_of_range error) {
            std::cout << "\nData a fost specificata gresit: \n";
            continue;
        }
    } while (1);

    p.date = s; // daca am trecut cu bine de acest do while, inseamna ca data a fost specificata corect

    do {
        try {
            std::cout << "\nIntroduceti suma achitata: ";
            in >> x;

            if (in.fail()) {
                in.clear();
                in.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                throw std::runtime_error("FORMAT GRESIT");
            }
            if (x <= 0) {
                throw std::runtime_error("FORMAT GRESIT. SUMA TREBUIE SA FIE STRICT POZITIVA!");
            }

            break;

        } catch (std::runtime_error error) {
            std::cout << "\nA avut loc o eroare la citire: \n" << error.what() << "\n";
            continue;
        }
    } while (1);

    p.sum = x; // daca am iesit din do-while, inseamna ca am citit o suma valida
    return in;
}

std::ostream& operator << (std::ostream& o, const Plata& plata) {
    return plata.display(o);
}